<?php

namespace Ludo237\Traits\Tests;

use Ludo237\Traits\Benchmarkable;
use Ludo237\Traits\Tests\Stubs\CommandStub;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;

#[CoversClass(Benchmarkable::class)]
class BenchmarkableTest extends TestCase
{
    #[Test]
    public function it_allows_to_set_a_benchmark_time()
    {
        $command = new CommandStub;
        $command->handle();

        // We need to account for small perf issues
        $this->assertGreaterThanOrEqual(5.0, $command->total);
    }
}
