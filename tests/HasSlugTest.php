<?php

namespace Ludo237\Traits\Tests;

use Illuminate\Support\Str;
use Ludo237\Traits\HasSlug;
use Ludo237\Traits\Tests\Stubs\UserStub;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;

#[CoversClass(HasSlug::class)]
class HasSlugTest extends TestCase
{
    #[Test]
    public function it_has_a_sluggable_key()
    {
        $this->assertEquals('name', HasSlug::sluggableKey());
    }

    #[Test]
    public function it_has_a_separator()
    {
        $this->assertEquals('.', HasSlug::separator());
    }

    #[Test]
    public function it_has_a_slug_key()
    {
        $this->assertEquals('slug', HasSlug::slugKey());
    }

    #[Test]
    public function it_creates_a_slug_if_not_provided_when_creating()
    {
        $user = UserStub::query()->create(['name' => 'foo']);

        $slug = $user->getAttributeValue('slug');
        $slug = explode('.', $slug);

        $this->assertNotNull($slug);
        $this->assertEquals(8, strlen($slug[1]));
        $this->assertEquals(Str::slug($user->getAttributeValue('name')), $slug[0]);
    }

    #[Test]
    public function it_does_not_create_a_slug_if_provided_when_creating()
    {
        $user = UserStub::query()->create(['name' => 'foo', 'slug' => 'foo.bar_baz']);

        $this->assertEquals('foo.bar_baz', $user->getAttributeValue('slug'));
    }

    #[Test]
    public function it_creates_a_slug_if_not_provided_when_updating()
    {
        $user = UserStub::query()->create(['name' => 'foo', 'slug' => 'foo.bar_baz']);

        $user->update([
            'slug' => null,
        ]);

        $slug = $user->getAttributeValue('slug');
        $slug = explode('.', $slug);

        $this->assertNotNull($slug);
        $this->assertEquals(8, strlen($slug[1]));
        $this->assertEquals(Str::slug($user->getAttributeValue('name')), $slug[0]);

        // Better safe than sorry
        $user->update([
            'slug' => '',
        ]);

        $slug = $user->getAttributeValue('slug');
        $slug = explode('.', $slug);

        $this->assertNotNull($slug);
        $this->assertEquals(8, strlen($slug[1]));
        $this->assertEquals(Str::slug($user->getAttributeValue('name')), $slug[0]);
    }

    #[Test]
    public function it_does_not_create_a_slug_if_provided_when_updating()
    {
        $user = UserStub::query()->create(['name' => 'foo', 'slug' => 'foo.bar_baz']);

        $user->update([
            'name' => 'new name',
            'slug' => 'foo.baz',
        ]);

        $this->assertEquals('foo.baz', $user->getAttributeValue('slug'));
    }
}
